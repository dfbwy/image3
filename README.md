### Typora+PicGo/PicGo-Core+Gitee/Github配置图床
1. 使用PicGo-Core
[Typora自动上传图片配置，集成PicGo-Core，文件以时间戳命名](https://blog.csdn.net/in_the_road/article/details/105733292)
2. 使用PicGo
[PicGo一条龙：下载、安装、配置gitee、配置typora](https://blog.csdn.net/weixin_45525272/article/details/125387761)
3. 配置过程中出现的问题
[typora+github/gitee+picgo上传失败请重试解决方法](https://blog.csdn.net/m0_52228020/article/details/122539142)

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
